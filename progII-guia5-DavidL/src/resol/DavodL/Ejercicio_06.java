package resol.DavodL;

import java.util.Scanner;

public class Ejercicio_06 {

	public static void botellas() {
		double capacidadBot[] = {5, 3, 1, 0.75, 0.5, 0.25, 0.1};
		double precioBot[] = {32, 20, 8, 5, 3.9, 2.5, 1.8};
		double precioLitroProd = 0;
		double cantidad[] = new double[7];
		double resto = 0;
		double costoBot = 0;
		double total = 0;

		Scanner teclado = new Scanner(System.in);
		System.out.print("Ingrese producto: ");
		String producto = teclado.nextLine().toLowerCase();
		System.out.print("Ingrese cantidad de litros: ");
		double litrosRest = teclado.nextDouble();

		if (producto.equals("aceite")) {
			precioLitroProd = 200;
		} else if (producto.equals("lavandina")) {
			precioLitroProd = 160;
		} else if (producto.equals("cerveza")) {
			precioLitroProd = 120;
		} else {
			System.out.print("Ingrese una opci�n correcta.");
		}
		double litros = litrosRest;
		int i=0;
		while(litrosRest>0){		
			if(litrosRest>=capacidadBot[i]) {		
				resto = litros % capacidadBot[i];
				cantidad[i] = litros/capacidadBot[i];
				litrosRest=resto;
				costoBot = cantidad[i]*precioBot[i];					
			}
			i++;			
		}
		double totalProd = precioLitroProd * litros;
		total = costoBot + (precioLitroProd * litros);

		System.out.println("Pedido 1: costo botellas: " + costoBot + ". Costo producto: " + totalProd + ". Costo total: " + total);
		teclado.close();
	}

}