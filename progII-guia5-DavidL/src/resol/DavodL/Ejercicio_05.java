package resol.DavodL;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Ejercicio_05{


	private static final String Collection = null;

	public static void main(String[] args) {

		ArrayList<Integer> listaNumero = new ArrayList<Integer>();
		listaNumero.add(4);
		listaNumero.add(6);
		listaNumero.add(8);
		listaNumero.add(2);
		listaNumero.add(16);


		ArrayList<Integer> listaNumero2 = (ArrayList<Integer>) listaNumero.clone();


		Scanner entrada = new Scanner(System.in);
		System.out.println("Ingrese un n�mero a borrar: ");
		int n = entrada.nextInt();		


		System.out.println("Arreglo inicial " + listaNumero);
		borrado(listaNumero,n);
		System.out.println("Arreglo modificado " + listaNumero );

		System.out.println("---------------------------------");

		System.out.println("Arreglo inicial " + listaNumero2);
		multiplicarPosicion(listaNumero2,n);
		System.out.println("Arreglo multiplicado " + listaNumero2 );

		System.out.println("---------------------------------");

		System.out.println("Dos arreglos unidos y ordenados" + multiplica2(listaNumero, listaNumero2) );

		System.out.println("---------------------------------");

		System.out.println("Arreglo inicial " + listaNumero);
		System.out.println("Arregla Sumado " + sumarValores(listaNumero2));	

		System.out.println("---------------------------------");


		System.out.println("Ingrese a partir de que posicion desea invertir: ");
		n = entrada.nextInt();	
		entrada.close();


		System.out.println("Arreglo invertido en posicion desde la posicion "+ imprimirInverso(listaNumero,n));


	}

	public static ArrayList<Integer> borrado(ArrayList<Integer> listaNumero, int n) {

		for (int i =0; i<= listaNumero.size()-1; i++) {

			if (listaNumero.get(i) == n) {
				listaNumero.remove(i);
			}
		}
		return listaNumero;
	}


	public static ArrayList<Integer> multiplicarPosicion(ArrayList<Integer> listaNumero, int n) {

		for (int i =0; i<= listaNumero.size()-1; i++) {

			if (listaNumero.get(i) == n) {
				listaNumero.set(i, n*(i+1));
			}
		}
		return listaNumero;
	}

	public static ArrayList<Integer> multiplica2(ArrayList<Integer> a, ArrayList<Integer> b){

		for(Integer x : b ) {
			a.add(x);		
		}
		Collections.sort(a); 	
		return a;
	}

	public static int sumarValores(ArrayList<Integer> lista) {
		int tam = lista.size()-1;		
		if (tam == 0){
			return lista.get(tam);
		}
		else{			           		   
			return lista.remove(tam) + sumarValores(lista); 
		}
	}

	public static ArrayList imprimirInverso(ArrayList<Integer> lista1,int posicion ) {

		ArrayList<Integer> listaNumero3 = new ArrayList<Integer>();
		Collections.reverse(lista1);

		for (int i =0; i< lista1.size()-posicion; i++) {			
			listaNumero3.add(lista1.get(i));
		}
		return listaNumero3;

	}

}