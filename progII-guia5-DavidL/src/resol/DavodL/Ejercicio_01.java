package resol.DavodL;

import java.util.Scanner;

class Empleado {
	Integer sueldo;
	Integer asig;
	Integer deduc;
	float neto;
	public void cargarSueldo(int sueldo) {
		this.sueldo = sueldo;
	}
	public void cargarAsig(int asig) {
		this.asig = asig;
	}
	public void cargarDeduc(int deduc) {
		this.deduc = deduc;
	}
	public void calcularNeto(Empleado persona) {

		float sueldo=this.sueldo;
		float asig=this.asig;
		float deduc=this.deduc;
		
		float asignacion=sueldo * (asig/100);
		float deduccion=sueldo * (deduc/100);
		
		this.neto=sueldo+asignacion-deduccion;
}
}

public final class Ejercicio_01 {
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.println("Ingrese cantidad de empleados: ");
		int n = entrada.nextInt();
		
		Empleado personas[] = new Empleado[n];
		
		for (int i = 0; i < personas.length; i++) {
			personas[i] = new Empleado();			
			System.out.println("Ingrese sueldo del empleado " + i + ": ");
			int sueldo = entrada.nextInt();
			personas[i].cargarSueldo(sueldo);
			System.out.println("Ingrese asignación del empleado " + i + ": ");
			personas[i].cargarAsig(entrada.nextInt());
			System.out.println("Ingrese deducciones del empleado " + i + ": ");
			personas[i].cargarDeduc(entrada.nextInt());
			personas[i].calcularNeto(personas[i]);
		}	
		
		System.out.println("Empleado		Sueldo		Asignaciones		Deducciones		Neto");
		
		for (int i = 0; i < n; i++) {
			System.out.println(i +"			"+ personas[i].sueldo +"		"+ personas[i].asig + "			" + personas[i].deduc +"			" + personas[i].neto);
		}
		entrada.close();
	}
}