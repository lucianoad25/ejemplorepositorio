package resol.DavodL;

import java.util.Scanner;


public final class Ejercicio_02 {

	public static void main(String[] args) {

		System.out.print("Ingresa Cantidad litros deposito: ");
		Scanner teclado = new Scanner(System.in);
		int litros = teclado.nextInt();
		teclado.close();

		System.out.println("Cantidad km con "+litros+" en carretera normal= "+CalcularKM(litros,'n')+" km.");

		System.out.println("Cantidad km con "+litros+" en carretera desiguales= "+CalcularKM(litros,'d')+" km.");
		;
	}

	public static double CalcularKM (int litros, char camino) {

		camino=Character.toUpperCase(camino);
		double km=0;

		if (camino=='N') {
			km=litros*12.5;}

		if (camino=='D') {
			km=litros*10.869;}		

		return km;

	}
}