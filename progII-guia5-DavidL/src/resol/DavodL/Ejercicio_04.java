package resol.DavodL;

import java.util.Random;

public class Ejercicio_04 {

	public static void main(String[] args) {
		
		int notas[] = new int[9];
		int menor50 = 0;
		int entre50y80 = 0;
		int entre70y80 = 0;
		int mas80 = 0;
		
		for (int i = 0; i < notas.length; i++) {
			notas[i]=getRandomNumberInRange(0, 100);
			
			if (notas[i]<50) {
				menor50++;
			}
			if (notas[i]>50 && notas[i]<80) {
				entre50y80++;
			}
			if (notas[i]>70 && notas[i]<80) {
				entre70y80++;
			}
			if (notas[i]>80) {
				mas80++;
			}
		}
		
		System.out.println("Los estudiantes que obtuvieron una calificación menor a 50 son: "+ menor50);
		System.out.println("Los estudiantes que obtuvieron una calificación entre 50 y 80 son: "+ entre50y80);
		System.out.println("Los estudiantes que obtuvieron una calificación entre 70 y 80 son: "+ entre70y80);
		System.out.println("Los estudiantes que obtuvieron una calificación mayor a 80 son: "+ mas80);
	}
	
	private static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}
	

}